% Clear environment
clear
clc

% I'm developing this with the files common to all assignments in the
% directory above
addpath ../

% The time of an equinox
eq = 7347737.336;
planet = Earth(eq);

% The three ground stations
ground_station_1 = [67.858428, 20.966880, 385.8];
ground_station_2 = [5.251439, 52.804664, -14.7];
ground_station_3 = [27.762889, -15.6338, 205.1];
              
% A satellite (This is currently a GPS satellite)
satellite = Kepler(planet, 4.6922e-04, 26400027, 54.0868, 235.7663, 143.6546, 216.8838)

% Noise characteristics
dAngle = 1; %(+- 1 degrees)
dRange = 100; %(+- meters)

% Vector where we store observations
observations_1 = [0, 0, 0, 0];
observations_2 = [0, 0, 0, 0];
observations_3 = [0, 0, 0, 0];

t_0 = 7347737.336;
t_f = 7347737.336 + 24*60*60;
times = t_0:60:t_f;
for n=1:length(times)
    % t-eq because satellite is defined as seconds _since_ equinox
    eci_obs = satellite.ECI(times(n)-eq);
    [r_1, e_1, a_1] = planet.ECItoTopocentricHorizonObservation(eci_obs, ground_station_1(1), ground_station_1(2), ground_station_1(3), times(n));
    [r_2, e_2, a_2] = planet.ECItoTopocentricHorizonObservation(eci_obs, ground_station_2(1), ground_station_2(2), ground_station_2(3), times(n));
    [r_3, e_3, a_3] = planet.ECItoTopocentricHorizonObservation(eci_obs, ground_station_3(1), ground_station_3(2), ground_station_3(3), times(n));
    % It only can be observed if above the horizon
    % We add noise here too
    if e_1 > 10
        observations_1 = [observations_1; [r_1 + rand*dRange, e_1 + rand*dAngle, a_1 + rand*dAngle, times(n)]];
    end
    if e_2 > 10
        observations_2 = [observations_2; [r_2 + rand*dRange, e_2 + rand*dAngle, a_2 + rand*dAngle, times(n)]];
    end
    if e_3 > 10
        observations_3 = [observations_3; [r_3 + rand*dRange, e_3 + rand*dAngle, a_3 + rand*dAngle, times(n)]];
    end
end

% Show observations
hold on
figure (1)
plot(observations_1(:,3), observations_1(:,2), 'o')
title('Groundstation 1: Elevation vs Azimuth');
figure (2)
plot(observations_2(:,3), observations_2(:,2), 'o')
title('Groundstation 2: Elevation vs Azimuth');
figure (3)
plot(observations_3(:,3), observations_3(:,2), 'o')
title('Groundstation 3: Elevation vs Azimuth');

% Initial orbit estimations
r_1 = [0 0 0; 0 0 0; 0 0 0];
r_2 = [0 0 0; 0 0 0; 0 0 0];
r_3 = [0 0 0; 0 0 0; 0 0 0];
for i=2:4
    r_1(i,:) = planet.TopocentricHorizonObservationToECIPosition(observations_1(i,1:3), ground_station_1(1), ground_station_1(2), ground_station_1(3), observations_1(i, 4));
    r_2(i,:) = planet.TopocentricHorizonObservationToECIPosition(observations_2(i,1:3), ground_station_1(1), ground_station_1(2), ground_station_1(3), observations_2(i, 4));
    r_3(i,:) = planet.TopocentricHorizonObservationToECIPosition(observations_3(i,1:3), ground_station_1(1), ground_station_1(2), ground_station_1(3), observations_3(i, 4));
end
% Get R and V vectors
[r_est_1, v_est_1] = HerrickGibbs(planet, r_1(3,:), observations_1(3, 4), r_1(6,:), observations_1(6, 4), r_1(9,:), observations_1(9, 4));
[r_est_2, v_est_2] = HerrickGibbs(planet, r_2(3,:), observations_2(3, 4), r_2(6,:), observations_2(6, 4), r_2(9,:), observations_2(9, 4));
[r_est_3, v_est_3] = HerrickGibbs(planet, r_3(3,:), observations_3(3, 4), r_3(6,:), observations_3(6, 4), r_3(9,:), observations_3(9, 4));
% Convert these into orbits
orb_est_1 = Kepler(planet, r_est_1, v_est_1)
orb_est_2 = Kepler(planet, r_est_2, v_est_2)
orb_est_3 = Kepler(planet, r_est_3, v_est_3)

% z = zeros(length(t),3);
% % Constants that effect operation of function
% maximum_iterations = 1000;
% for n=1:length(t)
%     observation_eci = sat.ECI(t(n));
%     %[range, elevation, azimuth] = planet.ECItoTopocentricObservation(observation_eci, ground_station_lat, ground_station_long, ground_station_height, t(n));
%     %z(n,:) = [range, elevation, azimuth];
%     z(n,:) = observation_eci' + rand(1,3)*10;
% end
% 
% for iter=1:10
%     step = 1;
%     % Construct the Jacobian
%     H = zeros(length(t),6);
%     for n=1:length(t)
%         for i=1:6
%             x_step = zeros(1, 6);
%             x_step(i) = step;
%             x_step = x_step + [x_0(1,:), x_0(2,:)];
%             orbit = Kepler(planet, x_0(1,:), x_0(2,:));
%             orbit_step = Kepler(planet, x_step(1:3), x_step(4:6));
%             [r, v] = orbit.ECI(t(n));
%             [r_step, v_step] = orbit_step.ECI(t(n));
%             result = ([r_step; v_step] - [r; v])./step;
%             H(n,i) = result(i);
%         end
%     end
% 
%     dz = zeros(length(t),6);
%     for n=1:length(t)
%         orbit = Kepler(planet, x_0(1,:), x_0(2,:));
%         [observation, ~] = orbit.ECI(t(n));
%         %dz(n,:) = TopocentricHorizonObservationToECIPosition(planet, z(n,:), ground_station_lat, ground_station_long, ground_station_height, t(n)) - observation';
%         dz(n,:) = [z(n,:), 0, 0, 0] - [observation', 0, 0, 0];
%     end
% 
%     dx = ((H'*H)^-1)*(H'*dz);
%     x_0 = x_0 + dx(1:2,1:3);
% end
