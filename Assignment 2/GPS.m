function [position, error] = GPS(initial_guess, pseudoranges, satellite_positions)

% Constants that effect operation of function
maximum_iterations = 1000;

% Preallocate and zero temp variables
number_satellites = length(pseudoranges);
dP = zeros(number_satellites,1);
P = zeros(number_satellites,1);
H = zeros(number_satellites, 4);
position = initial_guess;

for iteration = 1:maximum_iterations
    % Construct Pseudorange matrixes
    for ii = 1:number_satellites
        element = 0;
        for jj = 1:3
            element = element + (satellite_positions(ii,jj) - position(jj))^2;
        end
        P(ii) = sqrt(element);
        dP(ii) = pseudoranges(ii) - (P(ii) + position(4));
    end
    % Construct H
    for ii = 1:number_satellites
        for jj = 1:3
            H(ii,jj) = -(satellite_positions(ii,jj) - position(jj))/P(ii);
        end
        H(ii,4) = 1;
    end
    % Update position
    dX = ((H'*H)^-1)*H'*dP;
    % Exit if we've lost convergance
    if (iteration > 1 && norm(dX) >= norm(old_dX))
        V = (H'*H)^-1;
        GDOP = sqrt(V(1,1)+V(2,2)+V(3,3)+V(4,4));
        PDOP = sqrt(V(1,1)+V(2,2)+V(3,3));
        HDOP = sqrt(V(1,1)+V(2,2));
        VDOP = sqrt(V(3,3));
        TDOP = sqrt(V(4,4));
        error = [GDOP, PDOP, HDOP, VDOP, TDOP];
        return;
    end
    position = position + dX;
    old_dX = dX;
end

% We couldn't converge :(
position = initial_guess;