% Clear environment
clear
clc

% I'm developing this with the files common to all assignments in the
% directory above
addpath ../

% Import satellite data
ephem_data = importdata('GPSsat_ephem.txt');
num_sats = length(ephem_data);

% Ground station data
ground_longitude = 150.03;
ground_latitude = -34.76;
ground_height = 680;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constants to plot satellite locations over time
time_0 = max(ephem_data(:,8)); % Plot for 12 hours after the latest epoch
time_step = 60;
time_f = time_0 + 12*60*60;

% Setup the plot we'll use to draw the sat orbits
% Initializing the Drawing Space
close all
set(gcf,'Menubar','default','Name','Orbit Visualization', 'NumberTitle','off','Position',[10,350,750,750], 'Color',[0.38 0.26 0.67]); 
lim=(1+max(ephem_data(:,3)))*max(ephem_data(:,2));%Setting the limits of the graph to fit the biggest satellites
clf
axis([-lim, lim, -lim, lim, -lim, lim])	
view(150,15) 
axis equal
shg
hold on
grid on
title('Orbital Visualization');

% Place the planet earth
planet = Earth(0);
[xx, yy, zz] = ellipsoid(0,0,0, planet.semimajor_axis, planet.semimajor_axis, planet.semiminor_axis);
load('topo.mat','topo','topomap1');
topo2 = [topo(:,181:360) topo(:,1:180)];
pro.FaceColor= 'texture';
pro.EdgeColor = 'none';
pro.Cdata = topo2;
earth= surface(xx,yy,zz,pro);
colormap(topomap1);
Xaxis= line([0 lim],[0 0],[0 0],'Color', 'red', 'Marker','.','LineStyle','-');
Yaxis= line([0 0],[0 lim],[0 0],'Color', 'red', 'Marker','.','LineStyle','-');

% Plotting the ECI Axes
line([0 lim],[0 0],[0 0],'Color', 'black', 'Marker','.','LineStyle','-')
line([0 0],[0 lim],[0 0],'Color', 'black', 'Marker','.','LineStyle','-')
line([0 0],[0 0],[0 lim],'Color', 'black', 'Marker','.','LineStyle','-')

% Plot the ground station
position_ground = planet.SurfacePosition(ground_latitude, ground_longitude, ground_height, time_0);
ground_handle = plot3(position_ground(1), position_ground(2), position_ground(3), 'o', 'MarkerEdgeColor', 'k','MarkerFaceColor','g','MarkerSize', 10);

% First create the satellites from data and store them in the matrix.
% Set their time to be time_0 and plot.
satellite_epochs = zeros(num_sats, 1);
sat_handle = zeros(num_sats, 1);
for n=1:num_sats
    satellite(n) = Kepler(planet, ephem_data(n, 3), ephem_data(n, 2), ephem_data(n, 4), ephem_data(n, 5), ephem_data(n, 6), ephem_data(n, 7));
    satellite_epochs(n) = ephem_data(n, 8);
    position = satellite(n).ECI(time_0-satellite_epochs(n)); % Since the it's time since epoch, not actual time
    sat_handle(n) = plot3(position(1), position(2), position(3), 'o', 'MarkerEdgeColor', 'k','MarkerFaceColor','r','MarkerSize', 6);
end
drawnow;

% Now plot them over the entire time, moving the dots and giving them
% trails
for time=time_0:time_step:time_f
    % Get the new position for all satellites
    for n=1:num_sats
        position = satellite(n).ECI(time-satellite_epochs(n));
        plot3(position(1), position(2), position(3) ,'o', 'MarkerEdgeColor', 'k','MarkerFaceColor','b','MarkerSize', 3);
        set(sat_handle(n), 'XData', position(1), 'YData', position(2), 'ZData', position(3));
    end
    % And then plot the new ground position
    position_ground = planet.SurfacePosition(ground_latitude, ground_longitude, ground_height, time);
    set(ground_handle, 'XData', position_ground(1), 'YData', position_ground(2), 'ZData', position_ground(3));
    drawnow;
end

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % NOTE, my current PART C is just changing the 
% % pointed to text file (F1 -> F2)
% 
%Import data for flight 1
flight = importdata('GPS_pseudorange_F1.txt');

% Setup some stuff we need to start iterating through the data
linenumber = 1;
datapoint = 1;
location_raw = [0, 0, 0, 0]';
location = [0, 0, 0, 0, 0];
pseudoranges = zeros(300, num_sats);
position = zeros(length(satellite),3);
satellite_azimuth_elevation = zeros(1,2);

% Iterate through the data line by line, joining events with the same
% timestamp
while linenumber <= length(flight)
    % Our time is
    time = flight(linenumber,1);
    % So therefore our basestation is
    position_ground = planet.SurfacePosition(ground_latitude, ground_longitude, ground_height, time);
    set(ground_handle, 'XData', position_ground(1), 'YData', position_ground(2), 'ZData', position_ground(3));
    rotate (earth, [0 0 1], planet.GreenwichMeanSiderealTime(flight(linenumber,1)));
    
    % Loop through all sats and propagate this time
    for sat_id=1:length(satellite)
        position(sat_id,:) = satellite(sat_id).ECI(time-satellite_epochs(sat_id))';
        set(sat_handle(sat_id), 'XData', position(sat_id,1), 'YData', position(sat_id,2), 'ZData', position(sat_id,3),'MarkerFaceColor','r');
    end
    
    % For each timestamp, build up all the single point positions
    sat_number = 1;
    pseudorange = [];
    gps_position = [];
    while (linenumber <= length(flight) && flight(linenumber,1) == time)
        pseudorange(sat_number,:) = flight(linenumber,3);
        gps_position(sat_number,:) = position(flight(linenumber,2),:);
        % Make the satellite we're curently using green
        set(sat_handle(flight(linenumber,2)), 'MarkerFaceColor','g');
        % Record the azimuth/elevation of all visible satellites to plot
        % later
        [~, elevation, azimuth] = planet.ECItoTopocentricHorizonObservation(position(flight(linenumber,2),:)', ground_latitude, ground_longitude, ground_height, time);
        satellite_azimuth_elevation = [satellite_azimuth_elevation; azimuth, elevation];
        linenumber = linenumber + 1;
        sat_number = sat_number + 1;
    end
    % Skip if we have less than 4 satellites
    if (sat_number - 1 >= 4)
        [location_raw, error_raw] = GPS(location_raw, pseudorange, gps_position);
        error(datapoint, :) = error_raw;
        % Calculate TPCH observation directions (From ground station) from
        % the GPS calculated ECI positions.
        [range, elevation, azimuth] = planet.ECItoTopocentricHorizonObservation(location_raw(1:3), ground_latitude, ground_longitude, ground_height, time);
        % A quick converson to LGCV, pretty easy given we have TPCH already
        north = range*cosd(elevation)*cosd(azimuth);
        east = range*cosd(elevation)*sind(azimuth);
        down = -range*sind(elevation);
        location(datapoint, :) = [time, north, east, down, location_raw(4)];
        % Also record how many satellites we had in view
        visibility(datapoint) = sat_number - 1;
        datapoint = datapoint + 1;
    end
    drawnow;
end

figure(2);
hold on;
real = importdata('UAVPosition_F1.txt');
plot3(location(:,2), location(:,3), location(:,4), 'b')
plot3(real(:,2), real(:,3), real(:,4), 'g')
title('Flight 1 Path');

figure(3);
plot(location(:,2), location(:,3))
title('Flight 1 Path Overhead');

figure(4);
plot(location(:,1), visibility);
title('Flight 1 Satellites Visible');

figure(5);
plot(satellite_azimuth_elevation(:,1), satellite_azimuth_elevation(:,2), 'o');
title('Flight 1 Satellite Azimuth/Elevations at Observations');

figure(6);
plot(location(:,1), location(:,5));
title('Flight 1 Clock Bias');

%[GDOP, PDOP, HDOP, VDOP, TDOP]
figure(7);
hold on
subplot(2,3,1);
plot(location(:,1), error(:,1));
title('Flight 1 GDOP');
subplot(2,3,2);
plot(location(:,1), error(:,2));
title('Flight 1 PDOP');
subplot(2,3,3);
plot(location(:,1), error(:,3));
title('Flight 1 HDOP');
subplot(2,3,4);
plot(location(:,1), error(:,4));
title('Flight 1 VDOP');
subplot(2,3,5);
plot(location(:,1), error(:,5));
title('Flight 1 TDOP');
subplot(2,3,6);
plot(location(:,1), visibility);
title('Flight 1 Visisble Satellites');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PART C %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE, my current PART C is just changing the 
% pointed to text file (F1 -> F2)

%Import data for flight 1
flight = importdata('GPS_pseudorange_F2.txt');

% Setup some stuff we need to start iterating through the data
linenumber = 1;
datapoint = 1;
location_raw = [0, 0, 0, 0]';
location = [0, 0, 0, 0, 0];
pseudoranges = zeros(300, num_sats);
position = zeros(length(satellite),3);
satellite_azimuth_elevation = zeros(1,2);

% Iterate through the data line by line, joining events with the same
% timestamp
while linenumber <= length(flight)
    % Our time is
    time = flight(linenumber,1);
    % So therefore our basestation is
    position_ground = planet.SurfacePosition(ground_latitude, ground_longitude, ground_height, time);
    set(ground_handle, 'XData', position_ground(1), 'YData', position_ground(2), 'ZData', position_ground(3));
    rotate (earth, [0 0 1], planet.GreenwichMeanSiderealTime(flight(linenumber,1)));
    
    % Loop through all sats and propagate this time
    for sat_id=1:length(satellite)
        position(sat_id,:) = satellite(sat_id).ECI(time-satellite_epochs(sat_id))';
        set(sat_handle(sat_id), 'XData', position(sat_id,1), 'YData', position(sat_id,2), 'ZData', position(sat_id,3),'MarkerFaceColor','r');
    end
    
    % For each timestamp, build up all the single point positions
    sat_number = 1;
    pseudorange = [];
    gps_position = [];
    while (linenumber <= length(flight) && flight(linenumber,1) == time)
        pseudorange(sat_number,:) = flight(linenumber,3);
        gps_position(sat_number,:) = position(flight(linenumber,2),:);
        % Make the satellite we're curently using green
        set(sat_handle(flight(linenumber,2)), 'MarkerFaceColor','g');
        % Record the azimuth/elevation of all visible satellites to plot
        % later
        [~, elevation, azimuth] = planet.ECItoTopocentricHorizonObservation(position(flight(linenumber,2),:)', ground_latitude, ground_longitude, ground_height, time);
        satellite_azimuth_elevation = [satellite_azimuth_elevation; azimuth, elevation];
        linenumber = linenumber + 1;
        sat_number = sat_number + 1;
    end
    % Skip if we have less than 5 satellites
    if (sat_number - 1 >= 6)
        [location_raw, error_raw] = GPS(location_raw, pseudorange, gps_position);
        error(datapoint, :) = error_raw;
        % Calculate TPCH observation directions (From ground station) from
        % the GPS calculated ECI positions.
        [range, elevation, azimuth] = planet.ECItoTopocentricHorizonObservation(location_raw(1:3), ground_latitude, ground_longitude, ground_height, time);
        % A quick converson to LGCV, pretty easy given we have TPCH already
        north = range*cosd(elevation)*cosd(azimuth);
        east = range*cosd(elevation)*sind(azimuth);
        % If the clock bias is lower than the average we see (~600)
        % then don't trust down, use the last one
        if location_raw(4) < 550
            down = location(n-1,4);
        else
            down = -range*sind(elevation);
        end
        location(datapoint, :) = [time, north, east, down, location_raw(4)];
        % Also record how many satellites we had in view
        visibility(datapoint) = sat_number - 1;
        datapoint = datapoint + 1;
    end
    drawnow;
end

figure(8);
hold on;
real = importdata('UAVPosition_F2.txt');
plot3(location(:,2), location(:,3), location(:,4), 'b')
plot3(real(:,2), real(:,3), real(:,4), 'g')
title('Flight 2 Path');

figure(9);
plot(location(:,2), location(:,3))
title('Flight 2 Path Overhead');

figure(12);
plot(location(:,1), location(:,5));
title('Flight 2 Clock Bias');