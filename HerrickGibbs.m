% Returns v2, given three timed position vectors.
% Uses pg13 wk3 from lecture slides
function [pos, vel] = HerrickGibbs(planet, r1, t1, r2, t2, r3, t3)
    % We solve around the middle position
    pos = r2;
    
    % Useful constants
    mu = planet.gravitational_constant*planet.mass;
    
    t12 = t2 - t1;
    t13 = t3 - t1;
    t23 = t3 - t2;
    g1 = t23/(t12*t13);
    g3 = t12/(t23*t13);
    g2 = g1 - g3;
    h1 = mu*t23/12;
    h3 = mu*t12/12;
    h2 = h1 - h3;
    d1 = g1 + h1/norm(r1)^3;
    d2 = g2 + h1/norm(r2)^3;
    d3 = g3 + h1/norm(r3)^3;
    
    % The crux of gibbs method
    vel = -d1*r1 + d2*r2+ d3*r3;
end