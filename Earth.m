classdef Earth < Planet
    methods (Access = public)
        function this = Earth(longitudinal_time_epoch)
            % WGS84 Reference Ellipsoid
            semimajor_axis = 6378137.0;
            semiminor_axis = 6356752.314245;
            % Wikipedia
            omega = 7.2921150*10^-5;
            % Adjusted mass for parameter in Assignment 2
            mass = 5.97258100284094e+024;
            
            % OMFE
            second_zonal_harmonic = 1.08263*10e-3;
            
            this@Planet(longitudinal_time_epoch, mass, semimajor_axis, semiminor_axis, omega, second_zonal_harmonic);
        end
        
        % GreenwichMeanSiderealTime calculates the sidereal time at a
        % certain time, returned as degrees
        %
        % Synopsis: GMST = GreenwichMeanSiderealTime()
        %
        % Output: GMST        = the sidereal time measured in degrees
        function GMST = GreenwichMeanSiderealTime(this, seconds_since_equinox)
            % Work out time
            Days = seconds_since_equinox/86400;
            GMST_Hours = mod(18.697374558+24.06570982441908*Days,24);
            GMST = GMST_Hours*(360/24);

            % Adjust as we know when the last equinox was
            Days = 7347737.336/86400;
            GMST_Hours = mod(18.697374558+24.06570982441908*Days,24);
            GMST = GMST - GMST_Hours*(360/24);
        end
    end
end