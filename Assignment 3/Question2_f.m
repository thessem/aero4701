% Propagates y for time t using state u
function [ y_prop ] = Question2_f( t, y, u )
odefun = @(t, y) [cos(u) + (1-2*y(1))*y(2), sin(u)]';
[~, y] = ode45(odefun, [0 t], y);
y_prop = y(end,:);
end