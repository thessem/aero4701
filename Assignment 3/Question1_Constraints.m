function [ c ] = Question1_Constraints( orbit, radius, velocity, eccentricity, inclination )

% I'm assuming that orbit and velocity are being specified at epoch 0
[r, v] = orbit.ECI(0);

c = [norm(r')/radius - 1;            % Desired radius
     norm(v')/velocity - 1;          % Desired velocity
     orbit.eccentricity - eccentricity;    % Eccentricity
     orbit.inclination - inclination];     % Inclination  
end

