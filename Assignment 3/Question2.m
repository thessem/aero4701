points = 5;
x = {1, degtorad(180), zeros(1,points)', [ones(points-1,1), zeros(points-1,1)]};
options = optimset('MaxFunEvals',1e7, 'MaxIter', 1e7);
x = fmincon(@(x)x(1), Question2_pack(x), [], [], [], [], [], [], @(x)Question2_c(Question2_unpack(x, points)), options);
%[~, y] = Question2_f(Question2_unpack(x, points), false);
x_f = Question2_unpack(x, points);
%plot(y(:,1), y(:,2));