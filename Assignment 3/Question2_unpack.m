function [ x_cell ] = Question2_unpack( x_array, n )
x_cell = {0, 0, zeros(1,n)', zeros(2,n-1)'};

x_cell{1} = x_array(1);
x_array = x_array(1+1:end);

x_cell{2} = x_array(1);
x_array = x_array(1+1:end);

x_cell{3} = x_array(1:n)';
x_array = x_array(1+n:end);

x_cell{4}(:,1) = x_array(1:n-1)';
x_array = x_array(1+n-1:end);

x_cell{4}(:,2) = x_array(1:n-1)';
end