clear, clc

% I'm developing this with the files common to all assignments in the
% directory above
addpath ../

% Construct parking orbit as per assignment
planet = Earth(0);
park_eccentricity = 0.2;
park_semimajor_axis = 6655937;
park_inclination = -28.5;
park_longitude_ascending_node = 0;
park_argument_periapsis = 0;
park_original_eccentric_anomaly = 0;
park_orbit = Kepler(planet, park_eccentricity, park_semimajor_axis, park_inclination, park_longitude_ascending_node, park_argument_periapsis, 0);

% Final orbit parameters (these are the constraints)
radius = 35786000;                     % (m), This is geostationary
velocity = sqrt(3.986005e14/radius);   % (m/s), Calculated using mu
eccentricity = 0.0;                      % These should
inclination = 0.0;                       % be obvious

% Initial Guess for Orbit (inputs and lambdas)
x = [1;                            % dE1
     1;                            % dVx1
     1;                            % dVy1
     1;                            % dVz1
     1;                            % dE2
     1;                            % dVx2
     1;                            % dVz3
     1];                           % dVz 
%x = x*100;
lambda = [1; 
          1;
          1;
          1];

% Iteration constants
alpha = 0.1;
penalty = 100;
threshold = 0.0001;
dx = 1e-5; % For numerical differentiation

% Get info about the planned orbit
orb = @(x) Question1_TransferOrbit( park_orbit, x(1), x(2:4)', x(5), x(6:8)' );
% Function to minimize
f = @(x) norm(x(2:4)) + norm(x(6:8));
% Constraints
c = @(x) Question1_Constraints(orb(x), radius, velocity, eccentricity, inclination);

% Steepest descent method
it = 1;
while (norm(c(x)) > threshold)
    % Form G by passing in an anon function that only returns one C function %
    G = zeros(length(lambda), length(x));
    selector = eye(length(lambda));
    for ii=1:length(lambda)
        G(ii,:) = Gradient(x, @(x) selector(ii,:)*c(x), dx);
    end
    G = G+(penalty/2)*(c(x)'*c(x));
    % Calculate hessian
    H = eye(8);
    for ii=1:length(lambda)
        H = H - lambda(ii)*eye(8);
    end
    gra = Gradient(x, f, dx);
    p = [H -G';-G zeros(length(lambda),length(lambda))]\[gra; c(x)];
    x = x + alpha*p(1:length(x));
    lambda = lambda + alpha*p((1:length(lambda))+length(x));
    it = it + 1;
    fprintf('Iteration %d: Convergance = %f\n', it, norm(c(x)));
end

% Save Orbits
[final_orbit, transfer_orbit] = orb(x);
% Fix up deltaE's
x(1) = mod(x(1), 360);
x(5) = mod(x(5), 360);

%%%%%%%%%%%%%%%%%%%%%%% PRINT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delta_V = f(x);
[v1_mag, v1_el, v1_az] = planet.GeocentricObservation(x(2:4));
[v2_mag, v2_el, v2_az] = planet.GeocentricObservation(x(6:8));
fprintf('deltaV = %3.1f m/s\n', delta_V);
fprintf('Manoeuvre 1: dV = %3.1f m/s, Vazimuth = %3.1f deg, Velevation = %3.1f deg, deltaEccentricity = %3.1f deg\n', v1_mag, v1_az, v1_el, x(1));
fprintf('Manoeuvre 2: dV = %3.1f m/s, Vazimuth = %3.1f deg, Velevation = %3.1f deg, deltaEccentricity = %3.1f deg\n', v2_mag, v2_az, v2_el, x(5));

%%%%%%%%%%%%% PLOT ORBITS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setup the graph, with appropriate limites and zoom
figure (1)
set(gcf,'Menubar','default','Name','Orbit', 'NumberTitle','off','Position',[10,350,750,750]); 
lim=(1+final_orbit.eccentricity)*final_orbit.semimajor_axis;
clf
axis([-lim, lim, -lim, lim, -lim, lim])	
view(150,15) 
axis equal
shg
hold on
grid on
title('Orbital');

% Add in the celestial axii
Xaxis= line([0 lim],[0 0],[0 0],'Color', 'red', 'Marker','.','LineStyle','-');
Yaxis= line([0 0],[0 lim],[0 0],'Color', 'red', 'Marker','.','LineStyle','-');

for eccentricity = 0:0.5:360
    r = park_orbit.ECIFromEccentricAnomaly(eccentricity);
    plot3 (r(1), r(2), r(3),'r-');
    if (eccentricity < x(1))
        plot3 (r(1), r(2), r(3),'ro','MarkerSize', 3);
    end
end

for eccentricity = transfer_orbit.mean_anomaly_epoch:0.5:transfer_orbit.mean_anomaly_epoch + 360
    r = transfer_orbit.ECIFromEccentricAnomaly(eccentricity);
    plot3 (r(1), r(2), r(3),'g-');
    if (eccentricity < transfer_orbit.mean_anomaly_epoch + x(5))
        plot3 (r(1), r(2), r(3),'go', 'MarkerSize', 3);
    end
end

for eccentricity = final_orbit.mean_anomaly_epoch:0.5:final_orbit.mean_anomaly_epoch+360
    r = final_orbit.ECIFromEccentricAnomaly(eccentricity);
    plot3 (r(1), r(2), r(3),'b-');
    if (eccentricity < final_orbit.mean_anomaly_epoch+20)
        plot3 (r(1), r(2), r(3),'bo', 'MarkerSize', 3);
    end
end

