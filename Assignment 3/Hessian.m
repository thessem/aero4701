function [ hes ] = Hessian( x, fun, h )
    s = size(x);   
    hes = zeros(s(1), s(1));
    for ii=1:s(1)
        h_mat = eye(s(1))*h;
        for jj=1:s(1)
            hes(ii,jj) = fun(x + h_mat(:,ii) + h_mat(:,jj))-fun(x + h_mat(:,ii))-fun(x + h_mat(:,jj))+fun(x);
        end
    end
    hes = hes./(h^2);
end
