% Given the parameters, calculate the resultant transfer orbit
function [orbit, transfer_orbit] = TransferOrbit( orbit, dE1, dV1, dE2, dV2 )
    % Calculate the starting position of transfer orbit
    [r, v] = orbit.ECIFromEccentricAnomaly(dE1);
    v = v + dV1';
    
    % Simulate transfer orbit
    transfer_orbit = Kepler(orbit.planet, r, v);
    
    % Calculate the final position of the transfer orbit
    [r, v] = transfer_orbit.ECIFromEccentricAnomaly(dE2);
    v = v + dV2';
    orbit = Kepler(orbit.planet, r, v);
end

