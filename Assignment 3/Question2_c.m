function [ c, ceq ] = Question2_c( x )
% The inequality constraint ensures time runs correctly
c = -x{1};

% Boundary Values
y_0 = [1, 0];
y_f = [0, 0];

% Number of shots
n = size((x{3}))*[1; 0];
m = size((x{4}))*[0; 1];

% Construct time matrix
t_f = x{1};
t_step = t_f/n;
t = 0:t_step:t_f;

% Initial shot
u = zeros(n+1,1);


% Calculate u
u(1,:) = x{2};
for ii=2:n+1
    u(ii,:) = u(ii-1,:) + t_step*x{3}(ii-1);
end

% Fire and measure defects
y = zeros(n,m);
y(1,:) = Question2_f(t_step, y_0, u(1));
for ii=1:m
    ceq(ii) = y(1,ii)  - x{4}(1,ii);
end
for ii=m:n
    y(ii,:) = Question2_f(t_step, x{4}(ii-1,:), u(ii));
        if ii < n
            for jj=1:m
                ceq((ii-1)*m+jj) = y(ii,jj)  - x{4}(ii,jj);
            end
        end
end
ceq(n*m) = norm(y(n,:) - y_f);
end
