function [ g ] = Gradient( x, fun, h )
    s = size(x);
    h_mat = eye(s(1))*h;
    g = zeros(s(1),1);
    for ii=1:s(1)
        g(ii) = (fun(x + h_mat(:,ii)) - fun(x - h_mat(:,ii)))/(2*h);
    end
end

