function [ x_array ] = Question2_pack( x_cell )
x_array = [x_cell{1}, x_cell{2}, x_cell{3}', x_cell{4}(:,1)', x_cell{4}(:,2)'];
end