%PLANET Data and functions specific to the planet being orbited around
%   This class contains constant data (shape, mass etc) of a planet, along
%   with providing functions used to calculate planet specific things (such
%   as relative positions of orbital locations in latitude/longitude.
classdef Planet
    % Constants that are true for every planet
    properties (Constant, GetAccess = public)
        % From CODATA, Units: m^3 kg^-1 s^-2
        gravitational_constant = 6.67384e-11
    end
    
    % Constants per planet
    properties (GetAccess = public, SetAccess = public)
        % Rotation of planet in degrees at t = 0
        longitudinal_time_epoch
        % Mass of planet, Units: kg
        mass
        % Ellipsoid Semi-major axis, Units: m
        semimajor_axis
        % Ellipsoid Semi-minor axis, Units: m
        semiminor_axis
        % Rotation rate, Units: rad s^-1
        omega
        %
        second_zonal_harmonic
    end
    
    methods (Access = public)
        function this = Planet(longitudinal_time_epoch, mass, major, minor, omega, second_zonal_harmonic)
            this.longitudinal_time_epoch = longitudinal_time_epoch;
            this.mass = mass;
            this.semimajor_axis = major;
            this.semiminor_axis = minor;
            this.omega = omega;
            this.second_zonal_harmonic = second_zonal_harmonic;
        end
        
        function ECI_pos = SurfacePosition(this, latitude, longitude, height, time) 
            height = height + this.semimajor_axis;
            longitude = mod(longitude + this.GreenwichMeanSiderealTime(time), 360);
            r_eci_x = (height)*cosd(latitude)*cosd(longitude);
            r_eci_y = (height)*cosd(latitude)*sind(longitude);
            r_eci_z = (height)*sind(latitude);
            ECI_pos = [r_eci_x, r_eci_y, r_eci_z]';
        end
        
        function [range, elevation, azimuth] = ECItoTopocentricHorizonObservation(this, observation, observer_lat, observer_long, observer_height, seconds_since_epoch)
            % Get relative ECI vector and then calculate angles
            p = observation - this.SurfacePosition(observer_lat, observer_long, observer_height, seconds_since_epoch);
            longitude = observer_long;
            latitude = observer_lat;
            QXx = [-sind(longitude)                cosd(longitude)                 0;
                   -sind(latitude)*cosd(longitude) -sind(latitude)*sind(longitude) cosd(latitude);
                   cosd(latitude)*cosd(longitude)  cosd(latitude)*sind(longitude)  sind(latitude)];
               
            r_TCPH = QXx*p;
            [range, elevation, ~] = this.GeocentricObservation(r_TCPH);
            CA = (r_TCPH(2)/range)/cosd(elevation);
            SA = (r_TCPH(1)/range)/cosd(elevation);
            azimuth = acosd(CA);
            if SA < 0
                azimuth = 360 - azimuth;
            end
        end
        
        function pos = TopocentricHorizonObservationToECIPosition(this, observation, observer_lat, observer_long, observer_height, seconds_since_epoch)
            H = observer_height;
            dec = asin(cosd(observer_lat)*cosd(observation(3))*cosd(observation(2)) + sind(observer_lat)*sind(observation(2)));
            h = acos((cosd(observer_lat)*sind(observation(2)) - sind(observer_lat)*cosd(observation(3))*cosd(observation(2)))/cos(dec)); 
            if (observation(3) > 0) && (observation(3) < 180)
                h = 2*pi - h;
            end
            RA = degtorad(observer_long) - h;
            Rho = [cos(RA)*cos(dec) sin(RA)*cos(dec) sin(dec)];
            pos = this.SurfacePosition(observer_lat, observer_long, H, seconds_since_epoch)' + observation(1)*Rho;
        end
        
        % Convert a position into these range variables
        function [range, declination, right_ascention] = GeocentricObservation(this, observation)
            range = norm(observation);
            declination = asind(observation(3)/range);
            if observation(2) > 0
                right_ascention = acosd((observation(1)/range)/cosd(declination));
            else
                right_ascention = 360 - acosd((observation(1)/range)/cosd(declination));
            end            
        end
        
         % Pretend we're a ground station measuring an ECI position
        function [range, elevation, azimuth] = ECItoTopocentricObservation(this, observation, observer_lat, observer_long, observer_height, seconds_since_epoch)
            % Get relative ECI vector and then calculate angles
            p = observation - this.SurfacePosition(observer_lat, observer_long, observer_height, seconds_since_epoch);
            [range, elevation, ~] = this.GeocentricObservation(p);
            CA = (p(2)/range)/cosd(elevation);
            SA = (p(1)/range)/cosd(elevation);
            azimuth = acosd(CA);
            if SA < 0
                azimuth = 360 - azimuth;
            end
        end
    end
end



