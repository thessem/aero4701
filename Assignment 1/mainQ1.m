clear ALL

% Simulated satellite is MOLNIYA 1-42, with orbit taken from 
% http://www.n2yo.com/satellite/?s=11007
% and http://nssdc.gsfc.nasa.gov/nmc/masterCatalog.do?sc=1978-080A
and http://www.satellite-calculations.com/TLETracker/SatTracker.htm
% and http://www.heavens-above.com/orbit.aspx?satid=11007&lat=0&lng=0&loc=Unspecified&alt=0&tz=UCT
% TLE is
% 1 11007U 78080A   14082.34771885  .00000204  00000-0  13542-2 0  5281
% 2 11007 062.5216 168.8446 7092023 251.0430 024.9017 02.31257923279788

planet = Earth(318.013423204422);
satellite = Kepler(planet, 0.7092023, 24154.697727509312e3, 62.5216, 168.8446, 251.0430, 24.9017);

time_0 = 0;
simulation_time = 60*60*24*1;
step_size = 1;

times = time_0:step_size:time_0+simulation_time;
ECI_positions = zeros([length(times),3]);
ground_track = zeros([length(times),3]);

for n = 1:length(times)
    % Earth fixed location for 3D plot
    ECI_positions(n,:) = satellite.ECI(times(n));
    % ECEF position encoded as lat/long for ground track
    [range, latitude, longitude] = satellite.GeocentricObservation(satellite.ECEF(times(n)));
    ground_track(n,:) = [range, latitude, longitude];
end

% Plotting code inspired by 
% https://github.com/deedydas/Satellite-Orbit-Simulation/blob/master/
% But I added comments, and changed the maths used in here to rely on my
% Kepler class
close all

% Setup the graph, with appropriate limites and zoom
figure (1)
set(gcf,'Menubar','default','Name','Orbit', 'NumberTitle','off','Position',[10,350,750,750]); 
lim=(1+satellite.eccentricity)*satellite.semimajor_axis;
clf
axis([-lim, lim, -lim, lim, -lim, lim])	
view(150,15) 
axis equal
shg
hold on
grid on
title('Orbital');

% Draw the planet earth into the plot
[xx, yy, zz]=ellipsoid (0,0,0, satellite.planet.semimajor_axis, satellite.planet.semimajor_axis, satellite.planet.semiminor_axis);
load('topo.mat','topo','topomap1');
topo2 = [topo(:,181:360) topo(:,1:180)];
pro.FaceColor= 'texture';
pro.EdgeColor = 'none';
pro.FaceLighting = 'phong';
pro.Cdata = topo2;
earth = surface(xx,yy,zz,pro);
colormap(topomap1)

% Rotate it an appropriate amount
rotation = satellite.planet.GreenwichMeanSiderealTime(0);
rotate (earth, [0 0 1], rotation);

% Add in the celestial axii, and plot
Xaxis= line([0 lim],[0 0],[0 0],'Color', 'red', 'Marker','.','LineStyle','-');
Yaxis= line([0 0],[0 lim],[0 0],'Color', 'red', 'Marker','.','LineStyle','-');
rotate (Xaxis, [0 0 1], rotation);
rotate (Yaxis, [0 0 1], rotation);
plot3 (ECI_positions(:,1), ECI_positions(:,2), ECI_positions(:,3),'r-', 'LineWidth', 2);

% Plot the groundtrack
figure (2);
set(gcf,'Name','Earth Track', 'NumberTitle','off','Position',[10,350,1000,500]); 
hold on
image([0 360],[-90 90],topo,'CDataMapping', 'scaled');
colormap(topomap1);
axis equal
axis ([0 360 -90 90]);
plot (ground_track(:,3),ground_track(:,2),'ro','MarkerFaceColor','r','MarkerSize', 2);