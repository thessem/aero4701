clear ALL

% Setup satellite
planet = Earth(318.013423204422);
satellite = Kepler(planet, 0.7092023, 24154.697727509312e3, 62.5216, 168.8446, 251.0430, 24.9017);

% Setup simulation variables
time_0 = 0;
simulation_time = 60*60*24*1;
step_size = 1;
times = time_0:step_size:time_0+simulation_time;
observations = zeros([length(times),3]);
visibility = zeros([length(times),1]);

% Tracking station parameters, Astana in Kazakhstan
ground_latitude = 51.1806;
ground_longitude = 71.4611;
minimum_elevation = 20;

% Run the simulation
for n = 1:length(times)
    [range, elevation, azimuth] = satellite.TopocentricHorizonObservation(ground_latitude, ground_longitude, 0, times(n));
    observations(n,:) = [range, elevation, azimuth];
    if elevation > minimum_elevation
        visibility(n) = 1;
    end
end

visibility_percent = (sum(sum(visibility))/length(times))*100;
max_elevation = max(max(observations(:,2)));

disp(['Satellite is visible ', num2str(visibility_percent),'% of the time, with a maximum elevation of ', num2str(max_elevation), ' degrees.']);

% Plot the graph
figure(3)
hold on
[ax, h1, h2] = plotyy(times, observations(:,1), times, observations(:,3));
hold(ax(2), 'on'); 
[h3]    = plot(ax(2), times, observations(:,2), [time_0, simulation_time], [minimum_elevation minimum_elevation]);
set(get(ax(1),'Xlabel'),'string','Time (Julian Date)')
set(get(ax(1),'Ylabel'),'string','Distance (m)')
set(get(ax(2),'Ylabel'),'string','Angle (deg)')
legend([h1;h2;h3],{'Range (Red)','Azimuth (Blue)','Elevation (Green)'})
set(h1,'LineStyle','-');
set(h1,'Color','red');
set(h2,'LineStyle','-');
set(h2,'Color','blue');
set(h3,'LineStyle','-');
set(h3,'Color','green');
set(ax(2),'YLim',[-360 360])

% Take 3 timed (but close) observations in an area where the
% elevation is 70% of maximum, and predict the orbit
maximum_seperation = 60*60*3;
predicted_semimajor_axis = zeros([length(maximum_seperation),1]);
predicted_eccentricity = zeros([length(maximum_seperation),1]);

% Find where 70% of max elevation first occurs
ix = find(observations(:,2)>max_elevation*.7, 1);

for n = 1:maximum_seperation
    % Invent our three times
    t1 = times(ix);
    t2 = t1 + n;
    t3 = t2 + n;
    % Find positions and then run tracking
    r1 = satellite.ECI(t1);
    r2 = satellite.ECI(t2);
    r3 = satellite.ECI(t3);
    [pos, vel] = HerrickGibbs(planet, r1, t1, r2, t2, r3, t3);
    % Print predicted semimajor axis of predicted orbit
    orbit = Kepler(planet, pos, vel);
    predicted_semimajor_axis(n) = orbit.semimajor_axis;
    predicted_eccentricity(n) = orbit.eccentricity;
end

figure(4)
hold on
[ax] = plot([1:maximum_seperation], predicted_semimajor_axis./satellite.semimajor_axis, [1:maximum_seperation], predicted_eccentricity./satellite.eccentricity);
%set(get(ax(1),'Xlabel'),'string','Seconds between measurements (s)')
%set(get(ax(1),'Ylabel'),'string','Predicted Semi-major Axis/Actual')

