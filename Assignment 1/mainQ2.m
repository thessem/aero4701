% Code to genenrate plots used in question 2.
% Requires Kepler.m, which requires ECI.m, ECEF.m, Perifocal.m,
% CartesianCoords.m

%clc
%clear

sat = Kepler(0.0059073, 6921000, 97.3874, 138.3337, 167.7790, 192.4885, 2456379.67155);

% All of these are julian_dates
time_0 = 2456379.94652;
simulation_time = (1/86400)*60*60*24;
step_size = 1/(24*60);
times = time_0:step_size:time_0+simulation_time;

% Preallocating for speed (and also to make sure that old data doesn't
% sneak in
r = zeros(length(times));
range = zeros(length(times));
azimuth = zeros(length(times));
elevation = zeros(length(times));
visibility = zeros(length(times));

% Tracking station parameters
ground_latitude = 38.84;
ground_longitude = -104.79583;

% Run the simulation
for n = 1:length(times)
    sat.Propagate(times(n));
    sat_ECEF = ECEF(sat);
    ground_station_ECEF = sat.GroundStationECEF(ground_latitude, ground_longitude, times(n));
    TPCH = sat_ECEF.TopocentricHorizon(ground_station_ECEF);
    range(n) = TPCH.range;
    elevation(n) = TPCH.elevation;
    azimuth(n) = TPCH.azimuth;
    if elevation(n) > 0
        visibility(n) = 1;
    end
end

visibility_percent = (sum(sum(visibility))/length(times))*100;
max_elevation = max(max(elevation));

disp(['Satellite is above the horizon ', num2str(visibility_percent),' of the time, with a maximum elevation of ', num2str(max_elevation), '.']);

% Plot the graph
figure(3)
hold on
[ax h1 h2] = plotyy(times,range,times, azimuth);
hold(ax(2), 'on'); 
[h3]    = plot(ax(2),times,elevation);
set(get(ax(1),'Xlabel'),'string','Time (Julian Date)')
set(get(ax(1),'Ylabel'),'string','Distance (m)')
set(get(ax(2),'Ylabel'),'string','Angle (deg)')
legend([h1;h2;h3],{'Range (Red)','Azimuth (Blue)','Elevation (Green)'})
set(h1,'LineStyle','-');
set(h1,'Color','red');
set(h2,'LineStyle','-');
set(h2,'Color','blue');
set(h3,'LineStyle','-');
set(h3,'Color','green');
set(ax(2),'YLim',[-360 360])

sightings = [21, 24, 26];
% 
% % Spot the satellite at 3 points, and determine predicted ECEF coords
for n = 1:length(sightings)
    sat.Propagate(times(sightings(n)));
    sat_ECEF = ECEF(sat);
    ground_station_ECEF = sat.GroundStationECEF(ground_latitude, ground_longitude, times(sightings(n)));
    TPCH = sat_ECEF.TopocentricHorizon(ground_station_ECEF);
    estimated_position(n) = TPCH.ToCartesianCoords(ground_station_ECEF);
end
% This method is straight from the lecture slides
mu = sat.big_g*sat.mass;
t1 = times(sightings(1));
t2 = times(sightings(2));
t3 = times(sightings(3));
g1 = (t3-t2)/((t2-t1)*(t3-t1));
g3 = (t2-t1)/((t3-t2)*(t3-t1));
g2 = g1-g3;
h1 = mu*(t3-t2)/12;
h3 = mu*(t2-t1)/12;
h2 = h1 - h3;
d1 = g1 + h1/norm(estimated_position(1).r)^3;
d2 = g2 + h2/norm(estimated_position(2).r)^3;
d3 = g3 + h3/norm(estimated_position(3).r)^3;
v = d1*estimated_position(1).r + d2*estimated_position(2).r + d3*estimated_position(3).r;
estimated_speed = ECEF(v);

% Convert this back into an orbit
% Orbital mechanics for engineers, Appendix D.18
mu = sat.big_g*sat.mass;
V = estimated_speed.r;
R = estimated_position(n).r;
r = norm(R);
v = norm(V);
vr   = dot(R,V)/r;
H    = cross(R,V);
h    = norm(H);
incl = acos(H(3)/h);
N    = cross([0 0 1],H);
n    = norm(N);
if n ~= 0
    RA = acos(N(1)/n);
    if N(2) < 0
        RA = 2*pi - RA;
    end
else
    RA = 0;
end
E = 1/mu*((v^2 - mu/r)*R - r*vr*V);
e = norm(E);
if n ~= 0
    if e > eps
        w = acos(dot(N,E)/n/e);
        if E(3) < 0
            w = 2*pi - w;
        end
    else
        w = 0;
    end
else
    w = 0;
end
a = h^2/mu/(1 - e^2);
if e > eps
    TA = acos(dot(E,R)/e/r);
    if vr < 0
        TA = 2*pi - TA;
    end
else
    cp = cross(N,R);
    if cp(3) >= 0
        TA = acos(dot(N,R)/n/r);
    else
        TA = 2*pi - acos(dot(N,R)/n/r);
    end
end
a = h^2/mu/(1 - e^2);

