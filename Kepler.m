%# Kepler is an orbital simulation class
%#
%# Kepler is a class that facilitates the simulation of an orbit. It is
%# mostly concerned with calculating the position of an orbit at various
%# times, measured as seconds since an epoch. It implements the following 
%# properties and methods:
%#
%# PROPERTIES
%#    eccentricity - Orbital eccentricity
%#    semimajor_axis - Semimajor Axis of orbit (m)
%#    inclination - Inclination of orbit (degrees)
%#    longitude_ascending_node - Otherwise known as right ascention of
%#                               the ascending node (degrees)  
%#    argument_periapsis - Argument of Periapsis (degrees)
%#    epoch - Seconds since epoch that orbit was specified at (s)
%#    mean_anomaly_epoch - The mean anomaly of object when orbit is
%#                         specified (radians)
%#    planet - Planet that orbit is around. See help for `Planet`.
%# METHODS
%#    myMethod - sample method that calls doc
%#
classdef Kepler < handle
    
    % Constants that effect the operation of this class
    properties (Constant, GetAccess = private)
        % Tolerance used for the newton's method section
        tolerance = 1e-8;
    end
    
    % Orbital Parameters
    properties (GetAccess = public, SetAccess = public)
        % Initial Elements, given and able to be calculated from given.
        % This means that when this class is properly constructed, all
        % these properties should be filled out properly
        planet
        eccentricity
        semimajor_axis
        inclination
        longitude_ascending_node
        argument_periapsis
        mean_anomaly_epoch % Mean anomaly at specification (t = 0)
        eccentric_anomaly_epoch
    end
    
    methods (Access = private)
        % MeanToEccentric calculates the eccentric anomaly based off the
        % current mean anomaly
        function eccentric_anomaly = MeanToEccentric(this, mean_anomaly)
            % Simply newton-raphson the eccentric anomaly down until change
            % is less than the tolerance, indicating we're stable
            if mean_anomaly < pi
                eccentric_anomaly = degtorad(mean_anomaly) + this.eccentricity/2;
            else
                eccentric_anomaly = degtorad(mean_anomaly) - this.eccentricity/2;
            end
            change = this.tolerance;
            while abs(change) >= this.tolerance
                numerator = degtorad(mean_anomaly) - eccentric_anomaly + this.eccentricity*sin(eccentric_anomaly);
                denumerator = (1 - this.eccentricity*cos(eccentric_anomaly));
                change = numerator/denumerator;
                eccentric_anomaly = eccentric_anomaly + change;
            end
            eccentric_anomaly = radtodeg(eccentric_anomaly);
        end
        
        % EccentricToTrue calculates the true anomaly based off the
        % current eccentric anomaly
        function true_anomaly = EccentricToTrue(this, eccentric_anomaly)
            true_anomaly = atan2d((sind(eccentric_anomaly)*(1-this.eccentricity^2)^.5),(cosd(eccentric_anomaly)-this.eccentricity));
        end
        
    end
    
    methods (Access = public)
        % class constructor, does exactly what you'd expect
        function this = Kepler(planet, eccentricity, semimajor_axis, inclination, longitude_ascending_node, argument_periapsis, mean_anomaly_epoch)
            
            % Cheap hack for constructing an orbit from position and
            % velocity vectors, as matlab is a useless programming language
            % that doesn't even support constructor overloading and for
            % some reason lets me call functions even though I've only
            % supplied some of the arguments!
            % TODO: Add a rant about dynamic typing to this comment
            if nargin == 3
                this.planet = planet;
                % See above comment
                pos = eccentricity;
                vel = semimajor_axis;
                
                radial_velocity = dot(pos,vel)/norm(pos);
                angular_momentum = cross(pos,vel);
                this.inclination = acosd(angular_momentum(3)/norm(angular_momentum));
                
                % Calculate actual eccentricity
                mu = planet.gravitational_constant*planet.mass;
                eccentricity_vector = (1/(mu))*((norm(vel)^2 - mu/norm(pos))*pos - norm(pos)*radial_velocity*vel);
                this.eccentricity = norm(eccentricity_vector);
                
                % Calculate RAAN and argument of periapsis
                node_line = cross([0 0 1], angular_momentum);
                if norm(node_line) ~= 0
                   	this.longitude_ascending_node = acosd(node_line(1)/norm(node_line));
                    this.argument_periapsis = acosd(dot(node_line,eccentricity_vector)/norm(node_line)/this.eccentricity);
                    if (node_line(2) < 0)
                        this.longitude_ascending_node = 360 - this.longitude_ascending_node;
                    end
                    
                else
                    this.longitude_ascending_node = 0;
                    this.argument_periapsis = 0;
                end
                
                % Work out the mean anomaly from true anomaly
                true_anomaly = acos(dot(eccentricity_vector, pos)/this.eccentricity/norm(pos));
                if radial_velocity < 0
                    true_anomaly = 2*pi - true_anomaly;
                end
                eccentric_anomaly = 2*atan2(sin(true_anomaly/2)*sqrt(1-this.eccentricity), cos(true_anomaly/2)*sqrt(1+this.eccentricity));
                this.mean_anomaly_epoch = radtodeg(eccentric_anomaly - this.eccentricity*sin(eccentric_anomaly));
                
                this.semimajor_axis = norm(angular_momentum)^2/mu/(1-this.eccentricity^2);
            else
                % Do the obvious thisect construction
                this.planet = planet;
                this.eccentricity = eccentricity;
                this.semimajor_axis = semimajor_axis;
                this.inclination = inclination;
                this.longitude_ascending_node = longitude_ascending_node;
                this.argument_periapsis = argument_periapsis;
                this.mean_anomaly_epoch = mean_anomaly_epoch;
            end
            this.eccentric_anomaly_epoch = this.MeanToEccentric( this.mean_anomaly_epoch);
        end
       
        
        % Propagate the simulation to represent a state at time julian date
        function true_anomaly = TrueAnomaly(this, seconds_since_epoch)            
            mean_anomaly = this.mean_anomaly_epoch + radtodeg(sqrt(this.planet.gravitational_constant*this.planet.mass/this.semimajor_axis^3)*seconds_since_epoch);
            true_anomaly = this.EccentricToTrue(this.MeanToEccentric(mean_anomaly));
        end

        function [pos, vel] = Perifocal(this, seconds_since_epoch)
            true_anomaly = this.TrueAnomaly(seconds_since_epoch);

            r_common = (this.semimajor_axis*(1-this.eccentricity^2))/(1+this.eccentricity*cosd(true_anomaly));
            v_common = sqrt((this.planet.mass*this.planet.gravitational_constant)/(this.semimajor_axis*(1-this.eccentricity^2)));       
            
            pos = r_common*[cosd(true_anomaly), sind(true_anomaly), 0]';
            vel = v_common*[-sind(true_anomaly), this.eccentricity+cosd(true_anomaly), 0]';
        end
        
        % Class converter to convert the current coords into ECI
        function [pos, vel] = ECI(this, seconds_since_epoch)
            [pos, vel] = Perifocal(this, seconds_since_epoch);
            
            R1 = [ cosd(this.argument_periapsis)  sind(this.argument_periapsis)  0
                  -sind(this.argument_periapsis)  cosd(this.argument_periapsis)  0
                  0           0          1];
            R2 = [1 0                      0
                  0  cosd(this.inclination) sind(this.inclination)
                  0 -sind(this.inclination) cosd(this.inclination)];
            R3 = [ cosd(this.longitude_ascending_node)  sind(this.longitude_ascending_node)  0
                  -sind(this.longitude_ascending_node)  cosd(this.longitude_ascending_node)  0
                  0           0          1];
              
            QxX = (R1*R2*R3)';
            pos = QxX*pos;
            vel = QxX*vel;
        end
        
        % Class converter to convert the current coords into ECI at
        % a specific eccentric anomaly
        function [r, v] = ECIFromEccentricAnomaly(this, new_eccentric_anomaly)
            % An ungly hack for Assignment 3, and repeats a lot of code
            % above.
            % Calculates the new true anomaly from the given eccentric then
            % calculates the perifocal position, we then apply the ECI
            % transform to it.
            true_anomaly = EccentricToTrue(this, new_eccentric_anomaly);
            r_common = (this.semimajor_axis*(1-this.eccentricity^2))/(1+this.eccentricity*cosd(true_anomaly));
            v_common = sqrt((this.planet.mass*this.planet.gravitational_constant)/(this.semimajor_axis*(1-this.eccentricity^2)));       
            r_p = r_common*[cosd(true_anomaly), sind(true_anomaly), 0]';
            v_p = v_common*[-sind(true_anomaly), this.eccentricity+cosd(true_anomaly), 0]';
            
            R1 = [ cosd(this.argument_periapsis)  sind(this.argument_periapsis)  0
                  -sind(this.argument_periapsis)  cosd(this.argument_periapsis)  0
                  0           0          1];
            R2 = [1 0                      0
                  0  cosd(this.inclination) sind(this.inclination)
                  0 -sind(this.inclination) cosd(this.inclination)];
            R3 = [ cosd(this.longitude_ascending_node)  sind(this.longitude_ascending_node)  0
                  -sind(this.longitude_ascending_node)  cosd(this.longitude_ascending_node)  0
                  0           0          1];
              
            QxX = (R1*R2*R3)';
            r = (QxX*r_p);
            v = (QxX*v_p);
        end
        
        % Class converter to convert the current coords into ECEF
        function [pos, vel] = ECEF(this, seconds_since_epoch)            
            [pos, vel] = this.ECI(seconds_since_epoch);
            theta = this.planet.GreenwichMeanSiderealTime(seconds_since_epoch);
            
            Q = [ cosd(theta)  sind(theta)  0
                 -sind(theta)  cosd(theta)  0
                 0            0           1];
            pos = Q*pos;
            vel = Q*vel;
        end
        
        
    end
    
end

    