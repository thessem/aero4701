%# MYCLASS is a sample class
%# All this text will show up at the top of the help if you call 'doc myClassName'
%#
%# myClass is an example for documentation. It implements the following properties and methods:
%# PROPERTIES
%#    myProp - empty sample property (some more explanation could follow here)
%#
%# METHODS
%#    myMethod - sample method that calls doc
%#

classdef myClass

properties
    myProp = []; %# empty sample property
end %# properties

methods

%%# MYMETHOD -- use %% so that you can easily navigate your class file
function myMethod(obj)
%#MYMETHOD calls up the help for the object
%#
%# SYNOPSIS myMethod(obj)
%# INPUT obj: the object
%# OUTPUT none
%#
   try
      doc(class(obj))
   catch
      help(class(obj))
   end
   end %#myMethod
end %#methods

end %#myClass