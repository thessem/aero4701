%% Main function to generate tests
function tests = KeplerTest
    tests = functiontests(localfunctions);
end

function TrueAnomalyTest(testcase)
    % Adapted from Orbital Mechanics for Engineers Example 3.1
    orbit = Kepler(Earth(), 0.37255, (21000e3+9600e3)/2, 0, 0, 0, 0, 0);
    
    % The book only gives one decimal place
    anom = round(orbit.TrueAnomaly(10800)*10)/10;
    
    % Get angle to range (0, 360)
    if anom < 0
        anom = 360 + anom;
    end
    
    verifyEqual(testCase, anom, 193.2);
end

function PerifocalTest(testcase)
    % Adapted from Orbital Mechanics for Engineers Example 2.11
    angular_momentum = 6e10;
    eccentricity = 0.3;
    semimajor = (angular_momentum^2)/(Earth().mass*Earth().gravitational_constant*(1-eccentricity^2));
    
    % Calculate mean anomaly from given true anomaly
    t = 120;
    e = acos((eccentricity+cosd(t))/(1+eccentricity*cosd(t)));
    m = e - eccentricity*sin(e);
    mean_anomaly = radtodeg(m);
    
    orbit = Kepler(Earth(), eccentricity, semimajor, 0, 0, 0, mean_anomaly, 0);
    
    [pos, vel] = orbit.Perifocal(0);
    
    % Round for compare with book
    % blah blah blah
end

function ECITest(testcase)
    % Adapted from Orbital Mechanics for Engineers Example 4.3
    angular_momentum = 58.310e9;
    inclination = 153.2;
    raan = 255.3;
    eccentricity = 0.1712;
    arg_peri = 20.07;
    t = 28.45;
    
    % Calc our orbital parameters from these ones
    semimajor = (angular_momentum^2)/(Earth().mass*Earth().gravitational_constant*(1-eccentricity^2));
    e = acos((eccentricity+cosd(t))/(1+eccentricity*cosd(t)));
    m = e - eccentricity*sin(e);
    mean_anomaly = radtodeg(m);
    
    orbit = Kepler(Earth(), eccentricity, semimajor, inclination, raan, arg_peri, mean_anomaly, 0);
end

function ECEFandLatLongRange(testcase)
    % Adapted from Orbital Mechanics for Engineers Example 4.12
    orbit = Kepler(Earth(), 0.19760, 8350e3, 60, 270, 45, radtodeg(-1.9360), 0);
    
    [lat, long, range] = orbit.LatLongRange(45*60);
end